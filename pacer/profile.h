/*
 * profile.h
 *
 * created on: Jun 5, 2021
 * author: aasthakm
 *
 * ioctl message format for traffic indicator
 */

#ifndef __PROFILE_H__
#define __PROFILE_H__

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SME_IOCTL _IO('S', 0)

#define MAX_FILENAME_LEN 512

extern char *sme_ctrl_dev;
extern char *client_ip, *server_ip;
extern uint32_t client_port, server_port;

enum {
  MSG_T_IPPORT = 0,
  MSG_T_PROFILE,
  MSG_T_PMAP,
  MSG_T_PROF_ID,
  MSG_T_MARKER,
  MSG_T_DATA,
  MSG_T_PROF_REQ,
  MSG_T_PROF_RSP,
  MSG_T_PMAP_IHASH,
  MSG_T_PMAP_IHASH_HASHTABLE,
  MSG_T_SSL_HANDSHAKE,
  //Always add new formats after this comment
  MAX_NUM_MSG_TYPES
};

typedef struct {
  uint64_t cmd;
  uint64_t length;
} msg_hdr_t;

#define MSG_HDR_LEN sizeof(msg_hdr_t)

#define init_msg(buf, MSG, len_p) \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  memset(h, 0, sizeof(msg_hdr_t));  \
  h->cmd = MSG; \
  *len_p = &h->length;  \
}

#define get_msg_args_len(buf, len)  \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  len = h->length;  \
}

#define get_msg_cmd(buf, msg_type)  \
{ \
  msg_hdr_t *h = (msg_hdr_t *) buf; \
  msg_type = h->cmd;  \
}

#define NUM_OCTETS64 (sizeof(uint64_t)/sizeof(uint8_t))

#define PROF_CONN_IP_HDR_LEN (4 * sizeof(uint32_t))
#define PROF_CONN_PORT_HDR_LEN (4 * sizeof(uint16_t))
#define PROF_CONN_IP_PORT_HDR_LEN PROF_CONN_IP_HDR_LEN + PROF_CONN_PORT_HDR_LEN

#define PROF_CONN_PMAP_HDR_LEN(n) \
  (sizeof(uint16_t) + ((n) * sizeof(uint64_t)))

/*
 * u16 extra slots[n]
 * u64 frames[n]
 * u64 spacing[n]
 * u64 latency[n]
 */
#define PROF_CONN_VAR_LEN_BUF_LEN(n)  \
  (((n) * sizeof(uint16_t)) + \
  ((n) * sizeof(uint64_t)) +  \
  ((n) * sizeof(uint64_t)) +  \
  ((n) * sizeof(uint64_t)))

#define PROF_CONN_LEN(n)  \
  ((2*sizeof(uint16_t)) + PROF_CONN_VAR_LEN_BUF_LEN(n))

//added the size of ihash_t instead of uint16_t for the new profile_id
#define PROF_CONN_LEN_IHASH(n)  \
((sizeof(ihash_t) + sizeof(uint16_t)) + PROF_CONN_VAR_LEN_BUF_LEN(n))

//added the size of ihash_t instead of uint16_tfor the new profile_id
// added an additional hashtable offset  for linking the profiles 
#define PROF_CONN_LEN_IHASH_HASHTABLE(n)  \
((sizeof(ihash_t) + sizeof(uint16_t)) + sizeof(hashtable_offset_t) + PROF_CONN_VAR_LEN_BUF_LEN(n))

#define PACER_MARKER_HASH_BINLEN  16

/*
 * length of the marker hash prefixed to the key in a GET request
 * request format: hex,key (comma sent in ascii format too)
 */
#define PACER_MARKER_HASH_HEXLEN 32

typedef struct ihash {
  char byte[PACER_MARKER_HASH_BINLEN];
} ihash_t;

void dbg_prt(char *buf, int buf_len);

void prt_hash(char *hbuf, int hlen, char *out);

void prepare_profile_marker_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint32_t num_public, uint32_t num_private, uint16_t hash_len,
    ihash_t *pubhash, ihash_t *privhash);

void print_profile_marker_buf(char *buf, int buf_len);

void create_tmp_profile(char **buf, int *buf_len, char *server_ip, int server_port,
    char *client_ip, int client_port, int client_sock, char *fname, int read);

void create_tmp_profile_id2(char **buf, int *buf_len, char *server_ip,
    int server_port, char *client_ip, int client_port, int client_sock,
    uint16_t prof_id);

void create_tmp_profile_map(char **buf, int *buf_len, char *ip1, int port1,
    char *ip2, int port2, char *ip3, int port3, char *ip4, int port4,
    int cilent_sock, uint16_t n_prof, char *fname, int read);
void create_tmp_profile_id(char **buf, int *buf_len, char *ip1, int port1,
    char *ip2, int port2, char *ip3, int port3, char *ip4, int port4,
    int client_sock, int prof_id);
void create_tmp_profile_marker_h64(char **buf, int *buf_len,
    char *src_ip, int src_port, char *in_ip, int in_port,
    char *out_ip, int out_port, char *dst_ip, int dst_port,
    uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash);

void create_profile_map(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, uint16_t *profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency);

void create_profile_map_ihash(char **buf, int *buf_len,
    char *ip1, uint16_t port1, char *ip2, uint16_t port2,
    char *ip3, uint16_t port3, char *ip4, uint16_t port4,
    uint16_t n_prof, char **profile_id, uint16_t *num_out_reqs,
    uint16_t **num_extra_slots, uint64_t **num_out_frames,
    uint64_t **spacing, uint64_t **first_frame_latency);

void print_char(char c);

void free_buf(char *buf, int buf_len);

#endif /* __DUMMY_PROFILE_H__ */
