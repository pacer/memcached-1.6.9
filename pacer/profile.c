/*
 * dummy_profile.c
 *
 * created on: Mar 28, 2017
 * author: aasthakm
 *
 * small randomly generated profile serialized into a buffer
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "profile.h"

#ifndef MAX_BUF_SIZE
#define MAX_BUF_SIZE 512
#endif

#define DBG 0

#define MAX_BUF_LEN 4096

char *sme_ctrl_dev = "/dev/SMECTRL";
int sme_ctrl_fd = 0;

char *client_ip = "139.19.171.101";
uint32_t client_port = 0;
char *server_ip = "139.19.171.103";
uint32_t server_port = 11211;

void dbg_prt(char *buf, int buf_len)
{
  int it = 0;
  int s_it = 0;
  char sbuf[MAX_BUF_LEN];
  memset(sbuf, 0, MAX_BUF_LEN);
  for (it = 0; it < buf_len; it++) {
    sprintf(sbuf+s_it, "%02X", *(unsigned char *) (buf+it));
    s_it += 2;
  }
  //sprintf(sbuf+s_it, "'\0'");
  printf("%d: %s\n", buf_len, sbuf);
}

void prt_hash(char *hbuf, int hlen, char *out)
{
  if (!hbuf || !hlen || !out)
    return;

  int i;
  for (i = 0; i < hlen; i++) {
    sprintf(out + (i*2), "%02x", *(unsigned char *) (hbuf + i));
  }
}

/*
 * pubhash is a single buffer containing num_pub
 * serialized hash entries. similarly, privhash
 */
void prepare_profile_marker_buf(char *buf, uint64_t *buf_len,
    uint32_t src_ip, uint32_t in_ip, uint32_t out_ip, uint32_t dst_ip,
    uint16_t src_port, uint16_t in_port, uint16_t out_port, uint16_t dst_port,
    uint32_t num_pub, uint32_t num_priv, uint16_t hash_len,
    ihash_t *pubhash, ihash_t *privhash)
{
  int i;
  uint16_t off = 0;
  char *p = NULL;
  char *it = NULL;

  off = 0;
  p = buf;

  ((uint32_t *) (p + off))[0] = src_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = in_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = out_ip;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = dst_ip;
  off += sizeof(uint32_t);
  ((uint16_t *) (p + off))[0] = src_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = in_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = out_port;
  off += sizeof(uint16_t);
  ((uint16_t *) (p + off))[0] = dst_port;
  off += sizeof(uint16_t);

  ((uint32_t *) (p + off))[0] = num_pub;
  off += sizeof(uint32_t);
  ((uint32_t *) (p + off))[0] = num_priv;
  off += sizeof(uint32_t);

  /*
   * hash(public inputs#, private inputs#)
   * acts as a unique request ID
   */
  if (num_pub > 0) {
    it = pubhash[0].byte;
    memcpy(p + off, it, (hash_len/2));
  }
  off += (hash_len/2);
  if (num_priv > 0) {
    it = privhash[num_priv-1].byte;
    memcpy(p + off, it, (hash_len/2));
  }
  off += (hash_len/2);

  // spare field for timestamp in kernel
  off += sizeof(uint64_t);

  for (i = 0; i < num_pub; i++) {
    it = pubhash[i].byte;
    memcpy(p + off, it, hash_len);
    off += hash_len;
    it += hash_len;
  }

  for (i = 0; i < num_priv; i++) {
    it = privhash[i].byte;
    memcpy(p + off, it, hash_len);
    off += hash_len;
    it += hash_len;
  }

  *buf_len = off;
}

void print_profile_marker_buf(char *buf, int buf_len)
{
  int i;
  int off = 0;
  uint16_t src_port, in_port, out_port, dst_port;
  uint32_t num_pub, num_priv;
  ihash_t *pubhash = NULL, *privhash = NULL;
  ihash_t reqid;
  struct sockaddr_in src_addr, in_addr, out_addr, dst_addr;
  char sip[INET_ADDRSTRLEN];
  char iip[INET_ADDRSTRLEN];
  char oip[INET_ADDRSTRLEN];
  char dip[INET_ADDRSTRLEN];
  memset(&src_addr, 0, sizeof(struct sockaddr_in));
  memset(&in_addr, 0, sizeof(struct sockaddr_in));
  memset(&out_addr, 0, sizeof(struct sockaddr_in));
  memset(&dst_addr, 0, sizeof(struct sockaddr_in));

  src_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[0];
  in_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[1];
  out_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[2];
  dst_addr.sin_addr.s_addr = ((uint32_t *) (buf + off))[3];
  off += (4 * sizeof(uint32_t));

  src_port = ntohs(((uint16_t *) (buf + off))[0]);
  in_port = ntohs(((uint16_t *) (buf + off))[1]);
  out_port = ntohs(((uint16_t *) (buf + off))[2]);
  dst_port = ntohs(((uint16_t *) (buf + off))[3]);
  off += (4 * sizeof(uint16_t));

  inet_ntop(AF_INET, &(src_addr.sin_addr), sip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(in_addr.sin_addr), iip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(out_addr.sin_addr), oip, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &(dst_addr.sin_addr), dip, INET_ADDRSTRLEN);
  printf("SRC: %s, %u, %u, IN: %s, %u, %u, OUT: %s, %u, %u, DST: %s, %u, %u\n",
      sip, src_addr.sin_addr.s_addr, (uint32_t) src_port,
      iip, in_addr.sin_addr.s_addr, (uint32_t) in_port,
      oip, out_addr.sin_addr.s_addr, (uint32_t) out_port,
      dip, dst_addr.sin_addr.s_addr, (uint32_t) dst_port);

  num_pub = ((uint32_t *) (buf + off))[0];
  num_priv = ((uint32_t *) (buf + off))[1];
  off += (2 * sizeof(uint32_t));

  printf("#pub: %d, #priv: %d\n", num_pub, num_priv);

  memset(&reqid, 0, sizeof(ihash_t));
  memcpy(reqid.byte, buf + off, sizeof(ihash_t));
  off += sizeof(ihash_t);
  dbg_prt(reqid.byte, sizeof(ihash_t));

  pubhash = (ihash_t *) malloc(sizeof(ihash_t)*num_pub);
  privhash = (ihash_t *) malloc(sizeof(ihash_t)*num_priv);
  for (i = 0; i < num_pub; i++) {
    memcpy(&pubhash[i], buf + off, sizeof(ihash_t));
    dbg_prt(pubhash[i].byte, sizeof(ihash_t));
    printf("int version: %u\n", ((uint8_t *) pubhash[i].byte)[0]);
    off += sizeof(ihash_t);
  }

  for (i = 0; i < num_priv; i++) {
    memcpy(&privhash[i], buf + off, sizeof(ihash_t));
    dbg_prt(privhash[i].byte, sizeof(ihash_t));
    off += sizeof(ihash_t);
  }

  if (pubhash)
    free(pubhash);
  if (privhash)
    free(privhash);
  pubhash = privhash = NULL;
}

void create_tmp_profile_marker_h64(char **buf, int *buf_len,
    char *src_ip, int src_port, char *in_ip, int in_port,
    char *out_ip, int out_port, char *dst_ip, int dst_port,
    uint32_t num_pub, uint32_t num_priv,
    ihash_t *pubhash, ihash_t *privhash)
{
  struct sockaddr_in srcaddr, inaddr, outaddr, dstaddr;
  uint32_t sip, iip, oip, dip;
  uint16_t sport, iport, oport, dport;
  uint64_t *msg_len = NULL;
  memset(&srcaddr, 0, sizeof(struct sockaddr_in));
  memset(&inaddr, 0, sizeof(struct sockaddr_in));
  memset(&outaddr, 0, sizeof(struct sockaddr_in));
  memset(&dstaddr, 0, sizeof(struct sockaddr_in));
  inet_aton(src_ip, &srcaddr.sin_addr);
  inet_aton(in_ip, &inaddr.sin_addr);
  inet_aton(out_ip, &outaddr.sin_addr);
  inet_aton(dst_ip, &dstaddr.sin_addr);
  srcaddr.sin_port = htons(src_port);
  inaddr.sin_port = htons(in_port);
  outaddr.sin_port = htons(out_port);
  dstaddr.sin_port = htons(dst_port);

  sip = srcaddr.sin_addr.s_addr;
  iip = inaddr.sin_addr.s_addr;
  oip = outaddr.sin_addr.s_addr;
  dip = dstaddr.sin_addr.s_addr;
  sport = srcaddr.sin_port;
  iport = inaddr.sin_port;
  oport = outaddr.sin_port;
  dport = dstaddr.sin_port;

#if DBG
  printf("%s:%d src: %s %u %u, in: %s %u %u, out: %s %u %u, dst: %s %u %u\n"
      , __func__, __LINE__
      , inet_ntoa(srcaddr.sin_addr), sip, ntohs(sport)
      , inet_ntoa(inaddr.sin_addr), iip, ntohs(iport)
      , inet_ntoa(outaddr.sin_addr), oip, ntohs(oport)
      , inet_ntoa(dstaddr.sin_addr), dip, ntohs(dport)
      );
#endif

  int total_msg_len = MSG_HDR_LEN + PROF_CONN_IP_PORT_HDR_LEN
    + (2*sizeof(uint32_t)) /* num_pub, num_priv fields */
    + sizeof(ihash_t) /* req ID hash */
    + sizeof(uint64_t) /* spare field for marker timestamp in kernel */
    + (num_pub*sizeof(ihash_t)) /* list of public input hashes */
    + (num_priv*sizeof(ihash_t)) /* list of private input hashes */
    ;

  char *p = (char *) malloc(total_msg_len+1);
  memset(p, 0, total_msg_len+1);
  init_msg(p, MSG_T_MARKER, &msg_len);
  prepare_profile_marker_buf(p+MSG_HDR_LEN, msg_len, sip, iip, oip, dip,
      sport, iport, oport, dport, num_pub, num_priv, sizeof(ihash_t),
      pubhash, privhash);

  *buf_len = total_msg_len;
  *buf = p;
#if DBG
  print_profile_marker_buf(p+MSG_HDR_LEN, *msg_len);
#endif
}
