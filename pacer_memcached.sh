#!/bin/bash

vmip=$(hostname -I)
workdir=/local/sme/workspace/pacer/apps/memcached-1.6.9
cd $workdir
echo "Init memcache server $vmip ..."
./memcached -d -m 40960 -I 1024m -l $vmip -p 11211 -u nobody -E 1
date;
echo "Load dataset into memcache..."
php pacer/loadvideos.php > loadvids.out 2>&1;
date
